from setuptools import setup, find_packages
setup(name='celerybeatredis',
    version='0.1.2',
    description='Python celery beat redis',
    url='https://gitlab.com/fynd/celerybeatredis',
    author='Crucio Team',
    packages=find_packages(".", exclude="test"),
    install_requires=[
        
    ])
